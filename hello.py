import os
import subprocess

from flask import Flask

app = Flask(__name__)


def get_debug_div():
    hostname = os.uname()[1]
    port = os.environ.get("PORT", 5000)
    git_hash = str(subprocess.check_output(['git', 'rev-parse', 'HEAD']))
    return """
            <div style='text-align:left;font-size:20px;color:black'>
                DEBUG INFO:
                <br>
                Hostname: {}
                <br>
                Port: {}
                <br>
                Git Hash: {}
                <br>
            </div>
    """.format(hostname, port, git_hash)


@app.route('/')
def hello_world():
    html = """
        <html>
        <body>
            <div style='text-align:left;font-size:80px;color:green'>
                <br>
                Hello, World! First Prod Release!
                <br>
            </div>"""
    if os.environ.get("DEBUG", False):
        html += get_debug_div()
    html += """
        </body>
        </html>"""
    return html


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.environ.get("PORT", 5000))

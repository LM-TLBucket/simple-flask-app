import unittest

import hello


class TestHello(unittest.TestCase):
    def test_index(self):
        expected = """
        <html>
        <body>
            <div style='text-align:left;font-size:80px;color:green'>
                <br>
                Hello, World! First Prod Release!
                <br>
            </div>
        </body>
        </html>"""
        html = hello.hello_world()
        self.assertEqual(html, expected)
